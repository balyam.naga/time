values = []

#constant time
#timecomplexity - o(1)
#spacecomplexity - o(n)
values.append(1)

#timecomplexity - o(n)
#because first we have to move all the items which takes o(n) and then insert which takes o(1).
#o(n)+o(1) = o(n) 
values.insert(0, 0)


#timecomplexity - o(1)
values.remove(1)

#timecomplexity - o(n)
#again here we have to shift the items to the left
values.remove(0)

print(values)



#Unique visitors to the website
#We can use set to restrict the duplicates and count the number of unique visitors
uniqueip = set()
ip1 = 123456
ip2 = 123456
ip3 = 1233211
uniqueip.add(ip1)
uniqueip.add(ip2)
uniqueip.add(ip3)
print(uniqueip)



#Autocomplete
#We can use dictionaries to key,value pair 
#TImecomplexity - o(n) for lookup
search = input("enter the letter")
places = {}
places['a'] = ['agra', 'angara']
places['b'] = ['btm', 'bommenahalli']
places['c'] = ['cankaaa', 'calcutta']
print(places[search])